import {useState, useEffect} from 'react';

import Button from 'react-bootstrap/Button';
// import usersData from '../data/usersData';
import UserCard from '../components/UserCard';
import {Link} from 'react-router-dom';


export default function Users() {

	// to store the users retrieved from the database
	const [users, setUsers] = useState([]);

	

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setUsers(data.map(user => {
				return(
					<UserCard key={user._id} user={user} />
				)
			}))
		})
	}, [])

	return (
		<>
		<>
		<div className="mb-2 text-center t-12">
		</div>
      <div className="mb-2 text-center t-12">
        <Button variant="primary" size="lg" as={Link} to={`/products/create`}>
          Add Product
        </Button>{' '}
        <Button variant="secondary" size="lg"as={Link} to={`/products/all`} >
          All Products
        </Button>
      </div>
      
    </>
		{users}
		</>
	)
}

/*
PROPS
Way to declare props drilling
We can pass information from one component to another using props (props drilling
Curly braces {} are used for props to signify that we are providing/passing information from one component to another using JS expression
*/


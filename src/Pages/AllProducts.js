import {useState, useEffect} from 'react';
import {Table } from 'react-bootstrap';


// import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';

export default function Products() {

	// to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	// console.log(productsData);

	// const products = productsData.map(product => {
	// 	return (
	// 		< ProductCard key={product.id} product = {product} />
	// 	)
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, [])

	return (
		<>
<Table striped bordered hover>

        <thead>
          <tr>
            
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Product Price</th>
            <th>Stocks</th>
            <th>Product Status</th>
            <th>Action</th>
          </tr>
        </thead>
 </Table>
		{products}
		</>
	)
}

/*
PROPS
Way to declare props drilling
We can pass information from one component to another using props (props drilling
Curly braces {} are used for props to signify that we are providing/passing information from one component to another using JS expression
*/

